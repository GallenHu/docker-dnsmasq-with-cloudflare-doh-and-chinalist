#!/bin/sh
# 每次启动时更新配置
curl -o /etc/dnsmasq.d/confs.tar -sSL https://file.199100.xyz/dist/dnsmasqconf.tar
tar -xzvf /etc/dnsmasq.d/confs.tar -C /etc/dnsmasq.d/
rm /etc/dnsmasq.d/confs.tar

# 启动
supervisord -n -c /etc/supervisor/supervisord.conf